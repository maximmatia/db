<?php

namespace DB\Wrapper;

use DB\Helper\Connection\Driver\Statement;
use DB\Helper\Connection\MysqlConnection;
use DB\SqlQueryBuilder\Query\MysqlQuery;

class MysqlWrapper extends Wrapper implements SqlWrapperInterface
{
    // The current transaction level.
    private $transLevel = 0;

    public function __construct(MysqlConnection $connection)
    {
        parent::__construct($connection);
    }

    public function beginTransaction()
    {
        if ($this->transLevel == 0) {
            $ret = $this->connection->get()->beginTransaction();
            $this->transLevel++;

            return $ret;
        } else {
            $this->connection->get()->exec("SAVEPOINT LEVEL{$this->transLevel}");
            $this->transLevel++;

            return true;
        }
    }

    public function commit()
    {
        $this->transLevel--;
        if ($this->transLevel == 0) {
            return $this->connection->get()->commit();
        } else {
            $this->connection->get()->exec("RELEASE SAVEPOINT LEVEL{$this->transLevel}");

            return true;
        }
    }

    public function rollBack()
    {
        $this->transLevel--;

        if ($this->transLevel == 0) {
            return $this->connection->get()->rollBack();
        } else {
            $this->connection->get()->exec("ROLLBACK TO SAVEPOINT LEVEL{$this->transLevel}");

            return true;
        }
    }

    public function buildQuery(): MysqlQuery
    {
        return new MysqlQuery($this);
    }

    /**
     * @param $sql
     * @param array $arguments
     * @param array $bimdValues
     * @return \PDOStatement
     * @throws \Exception
     */
    public function executeQuery($sql, array $arguments = []): \PDOStatement
    {
        try {
            $stmt = $this->connection->get()->prepare($sql);
            $stmt->execute($arguments);

            return $stmt;
        } catch (\Exception $e) {
            if (substr_count($e->getMessage(), 'Deadlock found')) {
                sleep(1);
                return $this->executeQuery($sql, $arguments);
            }

            throw $e;
        }
    }

    public function lastInsertId()
    {
        return @array_pop($this->executeQuery("SELECT LAST_INSERT_ID()")->fetch());
    }
}
