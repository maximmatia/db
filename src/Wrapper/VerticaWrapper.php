<?php

namespace DB\Wrapper;

use DB\Helper\Connection\Driver\Statement;
use DB\Helper\Connection\VerticaConnection;
use DB\SqlQueryBuilder\Query\SqlQuery;

class VerticaWrapper extends Wrapper implements SqlWrapperInterface
{
    public function __construct(VerticaConnection $connection)
    {
        parent::__construct($connection);
    }

    public function buildQuery(): SqlQuery
    {
        return new SqlQuery($this);
    }

    public function executeQuery($sql, array $arguments = []): Statement
    {
        $stmt = $this->connection->get()->prepare($sql);
        $stmt->execute($arguments);

        return $stmt;
    }
}