<?php

namespace DB\Wrapper;

use DB\Helper\Connection\Driver\Statement;
use DB\Helper\Connection\MysqlConnection;
use DB\Helper\Connection\SqliteConnection;
use DB\SqlQueryBuilder\Query\MysqlQuery;
use DB\SqlQueryBuilder\Query\SqlQuery;

class SqliteWrapper extends Wrapper implements SqlWrapperInterface
{
    // The current transaction level.
    private $sem;

    public function __construct(SqliteConnection $connection)
    {
        parent::__construct($connection);

        $key = ftok($this->connection->getDb(), 'a');
        $this->sem = sem_get($key);

    }

    public function beginTransaction()
    {
        sem_acquire($this->sem);
        return $this->connection->get()->beginTransaction();
    }

    public function commit()
    {
        $success =$this->connection->get()->commit();
        sem_release($this->sem);
        return $success;
    }

    public function rollBack()
    {
        $success =$this->connection->get()->rollBack();
        sem_release($this->sem);
        return $success;
    }

    public function buildQuery(): SqlQuery
    {
        return new SqlQuery($this);
    }

    /**
     * @param       $sql
     * @param array $arguments
     *
     * @return Statement
     * @throws \Exception
     */
    public function executeQuery($sql, array $arguments = []): Statement
    {
        try {
            $stmt = $this->connection->get()->prepare($sql);
            $stmt->execute($arguments);

            return $stmt;
        } catch (\Exception $e) {
            if (substr_count($e->getMessage(), 'Deadlock found')) {
                sleep(1);
                return $this->executeQuery($sql, $arguments);
            }

            throw $e;
        }
    }

    public function lastInsertId()
    {
        $d = $this->executeQuery("SELECT LAST_INSERT_ID()")->fetch();
        return @array_pop($d);
    }
}