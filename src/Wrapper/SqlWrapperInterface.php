<?php

namespace DB\Wrapper;

use DB\Helper\Connection\Driver\Statement;

interface SqlWrapperInterface
{
    public function buildQuery();

    public function executeQuery($sql, array $arguments = []): \PDOStatement;
}