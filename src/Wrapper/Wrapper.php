<?php

namespace DB\Wrapper;

use DB\Helper\Connection\BasicConnection;

abstract class Wrapper
{
    public $connection;

    public function __construct(BasicConnection $connection)
    {
        $this->connection = $connection;
    }

    public function __call($name, $arguments)
    {
        return $this->connection->get()->$name(...$arguments);
    }
}