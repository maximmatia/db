<?php
/**
 * Created by PhpStorm.
 * User: Maxim
 * Date: 19.05.2017
 * Time: 15:38
 */

namespace DB;

use DB\Exception\DBException;
use DB\Helper\Connection\MysqlConnection;
use DB\Helper\Connection\RedisConnection;
use DB\Helper\Connection\SqliteConnection;
use DB\Helper\Connection\VerticaConnection;
use DB\Wrapper\MysqlWrapper;
use DB\Wrapper\RedisWrapper;
use DB\Wrapper\SqliteWrapper;
use DB\Wrapper\VerticaWrapper;

class DB
{
    private $wrappers = [];

    public function addMysql(MysqlConnection $connection, $alias = null)
    {
        $this->wrappers['mysql'][$alias] = new MysqlWrapper($connection);
    }

    public function addSqlite(SqliteConnection $connection, $alias = null)
    {
        $this->wrappers['sqlite'][$alias] = new SqliteWrapper($connection);
    }

    public function addVertica(VerticaConnection $connection, $alias = null)
    {
        $this->wrappers['vertica'][$alias] = new VerticaWrapper($connection);
    }

    public function addRedis(RedisConnection $connection, $alias = null)
    {
        $this->wrappers['redis'][$alias] = new RedisWrapper($connection);
    }

    public function mysql($alias = null): MysqlWrapper
    {
        if (isset($this->wrappers['mysql'][$alias])) {
            return $this->wrappers['mysql'][$alias];
        }
        throw new DBException('Connection is not set.');
    }

    public function vertica($alias = null): VerticaWrapper
    {
        if (isset($this->wrappers['vertica'][$alias])) {
            return $this->wrappers['vertica'][$alias];
        }
        throw new DBException('Connection is not set.');
    }

    public function redis($alias = null): RedisWrapper
    {
        if (isset($this->wrappers['redis'][$alias])) {
            return $this->wrappers['redis'][$alias];
        }
        throw new DBException('Connection is not set.');
    }

    public function sqlite($alias = null): RedisWrapper
    {
        if (isset($this->wrappers['sqlite'][$alias])) {
            return $this->wrappers['sqlite'][$alias];
        }
        throw new DBException('Connection is not set.');
    }
}