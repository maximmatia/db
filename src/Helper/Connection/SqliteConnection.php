<?php

namespace DB\Helper\Connection;

use DB\Helper\Connection\Driver\PDO\PDOConnection;
use PDO;

class SqliteConnection extends BasicConnection
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function connect(): PDOConnection
    {
        $dsn = 'sqlite:' . $this->db;

        return new PDOConnection($dsn);
    }

    /**
     * @return mixed
     */
    public function getDb()
    {
        return $this->db;
    }
}