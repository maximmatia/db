<?php

namespace DB\Helper\Connection;

use DB\Helper\Connection\Driver\ODBC\ODBCConnection;

class VerticaConnection extends BasicConnection
{
    const CHARSET = 'UTF8';

    private $host;
    private $db;
    private $user;
    private $pass;
    private $charset;
    private $options;

    public function __construct($host, $db, $user, $pass, $charset = self::CHARSET, $options = [])
    {
        $this->host = $host;
        $this->db = $db;
        $this->user = $user;
        $this->pass = $pass;
        $this->charset = $charset;
        $this->options = $options;
    }


    public function connect(): ODBCConnection
    {
        $dsn = 'Driver=Vertica;Server=' . $this->host . ';Database=' . $this->db . ';Server_CSet=' . $this->charset;

        return new ODBCConnection($dsn, $this->user, $this->pass, $this->options);
    }

    public function addOption($key, $value)
    {
        $this->options[$key] = $value;
    }
}