<?php

namespace DB\Helper\Connection;

abstract class BasicConnection
{
    protected $connection;

    abstract public function connect();

    /**
     * @return mixed
     */
    public function get()
    {
        if (is_null($this->connection)) {
            $this->connection = $this->connect();
        }

        return $this->connection;
    }
}