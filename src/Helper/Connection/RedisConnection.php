<?php

namespace DB\Helper\Connection;

use Redis;

class RedisConnection extends BasicConnection
{
    private $host;
    private $port;
    private $password;

    public function __construct($host, $port, $password = null)
    {
        $this->host = $host;
        $this->port = $port;
        $this->password = $password;
    }


    public function connect(): Redis
    {
        $redis = new Redis();
        $redis->connect($this->host, $this->port);
        if (!is_null($this->password)) {
            $redis->auth($this->password);
        }

        return $redis;
    }

}