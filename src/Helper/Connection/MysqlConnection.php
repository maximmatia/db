<?php

namespace DB\Helper\Connection;

use DB\Helper\Connection\Driver\PDO\PDOConnection;
use PDO;

class MysqlConnection extends BasicConnection
{
    const CHARSET = 'UTF8';
    const OPTIONS = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
        #PDO::ATTR_PERSISTENT         => true,
    ];

    private $host;
    private $db;
    private $user;
    private $pass;
    private $charset;
    private $options;

    public function __construct($host, $db, $user, $pass, $charset = self::CHARSET, $options = self::OPTIONS)
    {
        $this->host = $host;
        $this->db = $db;
        $this->user = $user;
        $this->pass = $pass;
        $this->charset = $charset;
        $this->options = $options;
    }


    public function connect(): PDO
    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->db . ';charset=' . $this->charset;

        return new PDO($dsn, $this->user, $this->pass, $this->options);
    }

    public function addOption($key, $value)
    {
        $this->options[$key] = $value;
    }
}