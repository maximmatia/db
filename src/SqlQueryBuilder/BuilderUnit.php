<?php

namespace DB\SqlQueryBuilder;

class BuilderUnit
{
    private $sql;
    private $placeholders;

    public function __construct($sql, array $placeholders = [])
    {
        $this->sql          = $sql;
        $this->placeholders = $placeholders;
    }

    /**
     * @return array
     */
    public function getPlaceholders(): array
    {
        return $this->placeholders;
    }

    /**
     * @param array $placeholders
     */
    public function setPlaceholders(array $placeholders)
    {
        $this->placeholders = $placeholders;
    }

    /**
     * @return mixed
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * @param mixed $sql
     */
    public function setSql($sql)
    {
        $this->sql = $sql;
    }
}

