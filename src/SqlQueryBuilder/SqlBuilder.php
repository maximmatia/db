<?php

namespace DB\SqlQueryBuilder;

use DB\Exception\InvalidArgumentException;
use DB\SqlQueryBuilder\Query\Elements\Field;
use DB\SqlQueryBuilder\Query\Elements\Insert;
use DB\SqlQueryBuilder\Query\SqlQuery;

class SqlBuilder implements BuilderInterface
{
    protected $from;
    protected $select;
    protected $join;
    protected $where;
    protected $groupBy;
    protected $orderBy;
    protected $limit;
    protected $delete;
    protected $update;
    protected $insert;
    protected $union;

    public function __construct(SqlQuery $query)
    {
        foreach ($query->toArray() as $key => $value) {
            if ($key == 'from') {
                $this->from = $value;
            } elseif ($key == 'select') {
                $this->select = $value;
            } elseif ($key == 'join') {
                $this->join = $value;
            } elseif ($key == 'where') {
                $this->where = $value;
            } elseif ($key == 'groupBy') {
                $this->groupBy = $value;
            } elseif ($key == 'orderBy') {
                $this->orderBy = $value;
            } elseif ($key == 'limit') {
                $this->limit = $value;
            } elseif ($key == 'delete') {
                $this->delete = $value;
            } elseif ($key == 'update') {
                $this->update = $value;
            } elseif ($key == 'insert') {
                $this->insert = $value;
            } elseif ($key == 'union') {
                $this->union = $value;
            }
        };
    }

    public function build(): BuilderUnit
    {
        $sql = '';
        $placeholders = $bindValues = [];

        if ($this->insert instanceof Insert) {
            $unit = $this->buildInsert($this->insert);
            $sql .= $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        } elseif (is_array($this->update) AND sizeof($this->update) > 0) {
            $sql = 'UPDATE';
        } elseif (!is_null($this->delete)) {
            $sql = 'DELETE ' . ($this->delete !== true ? $this->delete : '') . ' FROM';
        }

        #select
        if (is_array($this->select) AND sizeof($this->select) > 0) {
            $unit = $this->buildSelect($this->select);
            $sql .= (strlen($sql) > 0 ? ' ' : '') . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #from
        if (is_array($this->from) AND sizeof($this->from) > 0) {
            $unit = $this->buildFrom($this->from);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #join
        if (is_array($this->join) AND sizeof($this->join) > 0) {
            $unit = $this->buildJoin($this->join);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #set
        if (is_array($this->update) AND sizeof($this->update) > 0) {
            $unit = $this->buildUpdate($this->update);
            $sql .= ' SET ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #where
        if (is_array($this->where) AND sizeof($this->where) > 0) {
            $unit = $this->buildWhere($this->where);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #group by
        if (is_array($this->groupBy) AND sizeof($this->groupBy) > 0) {
            $unit = $this->buildGroupBy($this->groupBy);
            $sql .= ' ' . $unit->getSql();
        }

        #order by
        if (is_array($this->orderBy) AND sizeof($this->orderBy) > 0) {
            $unit = $this->buildOrderBy($this->orderBy);
            $sql .= ' ' . $unit->getSql();
        }

        #limit
        if (is_array($this->limit) AND sizeof($this->limit) > 0) {
            $unit = $this->buildLimit($this->limit);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #union
        if (is_array($this->union) AND sizeof($this->union) > 0) {
            $unit = $this->buildUnion($this->union);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        if ($this->insert instanceof Insert AND sizeof($this->insert->getOnDuplicateKeyUpdate()) > 0) {
            $unit = $this->buildInsertDuplicateKeyUpdate($this->insert);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());

        }

        return new BuilderUnit($sql, $placeholders);

    }

    protected function buildInsert(Insert $insert): BuilderUnit
    {
        $placeholders = [];
        $fields = array_map(function ($field) {
            return $field instanceof Field ? $field->build()->getSql() : $field;
        }, $insert->getFields());

        $sql = 'INSERT INTO ' . $insert->getTable();
        if (sizeof($fields) > 0) {
            $sql .= ' (' . implode(', ', $fields) . ')';
        }

        $values = $insert->getValues();
        if (sizeof($values) > 0) {
            $sql .= ' VALUES ' . implode(', ', array_fill_keys(array_keys($values),
                    '(' . implode(', ', array_fill_keys($fields, '?')) . ')'));
            $placeholders = array_merge($placeholders, ...$values);
        }

        return new BuilderUnit($sql, $placeholders);
    }

    protected function buildInsertDuplicateKeyUpdate(Insert $insert): BuilderUnit
    {
        $sql = $ph = [];
        if (sizeof($insert->getOnDuplicateKeyUpdate()) > 0) {
            foreach ($insert->getOnDuplicateKeyUpdate() as list($field, $value)) {
                if ($field instanceof Field) {
                    $field = $field->build()->getSql();
                }

                if ($value instanceof BuilderInterface) {
                    $valueUnit = $value->build();
                    $sql[] = $field . ' = ' . $valueUnit->getSql();
                    $ph = array_merge($ph, $valueUnit->getPlaceholders());
                } else {
                    $sql[] = $field . ' = VALUES(' . $field . ')';
                }
            }
        }

        return new BuilderUnit((sizeof($sql) > 0 ? 'ON DUPLICATE KEY UPDATE ' . implode(', ', $sql) : ''), $ph);
    }

    protected function buildSelect(array $select): BuilderUnit
    {
        #select
        if (sizeof($select) === 0) {
            throw new InvalidArgumentException('Incorrect part: select.');
        }

        $sql = $placeholders = [];
        foreach ($select as $field) {
            $unit = $field->build();
            $sql[] = $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        return new BuilderUnit('SELECT ' . implode(', ', $sql) . ' FROM', $placeholders);
    }

    protected function buildFrom(array $from): BuilderUnit
    {
        if (sizeof($from) === 0) {
            throw new InvalidArgumentException('Incorrect part: from.');
        }
        $sql = $placeholders = [];
        foreach ($from as list($table, $alias)) {
            if ($table instanceof BuilderInterface) {
                $tableUnit = $table->build();
                $table = '(' . $tableUnit->getSql() . ')';
                $placeholders = array_merge($placeholders, $tableUnit->getPlaceholders());
            }

            $sql[] = "{$table}" . ((isset($alias) AND $alias != $table) ? ' ' . $alias : '');
        }

        return new BuilderUnit(implode(', ', $sql), $placeholders);

    }

    protected function buildJoin(array $join): BuilderUnit
    {
        if (sizeof($join) === 0) {
            throw new InvalidArgumentException('Incorrect part: join.');
        }

        $sql = $placeholders = [];
        foreach ($join as $part) {
            $table = $part->getTable();
            if ($table instanceof BuilderInterface) {
                $tableUnit = $table->build();
                $table = '(' . $tableUnit->getSql() . ') ' . $part->getAlias();
                $placeholders = array_merge($placeholders, $tableUnit->getPlaceholders());
            }

            $unit = $part->getConditions()->build();
            $sql [] = $part->getType() . ' ' . $table . ' ' . $part->getAlias() . ' ON ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        return new BuilderUnit(implode(' ', $sql), $placeholders);

    }

    protected function buildUpdate(array $update): BuilderUnit
    {
        if (sizeof($update) === 0) {
            throw new InvalidArgumentException('Incorrect part: update.');
        }

        $sql = $placeholders = [];
        foreach ($update as list($field, $value)) {
            if ($field instanceof Field) {
                $field = $field->build()->getSql();
            }

            if ($value instanceof Field) {
                $valueUnit = $value->build();
                $sql[] = $field . ' = ' . $valueUnit->getSql();
                $placeholders = array_merge($placeholders, $valueUnit->getPlaceholders());
            } else {
                $sql[] = $field . ' = ?';
                $placeholders = array_merge($placeholders, [$value]);
            }
        }

        return new BuilderUnit(implode(', ', $sql), $placeholders);
    }

    protected function buildWhere(array $where): BuilderUnit
    {
        if (sizeof($where) === 0) {
            throw new InvalidArgumentException('Incorrect part: where.');
        }

        $sql = $placeholders = [];
        foreach ($where as $conditions) {
            $unit = $conditions->build();
            $sql[] = $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        return new BuilderUnit('WHERE ' . implode(' AND ', $sql), $placeholders);
    }

    protected function buildGroupBy(array $groupBy): BuilderUnit
    {
        if (sizeof($groupBy) === 0) {
            throw new InvalidArgumentException('Incorrect part: groupBy.');
        }

        $sql = 'GROUP BY ' . implode(', ', array_map(function ($item) {
                if ($item instanceof Field) {
                    $item = $item->build()->getSql();
                }

                return $item;
            }, $groupBy));

        return new BuilderUnit($sql, []);
    }

    protected function buildOrderBy(array $orderBy): BuilderUnit
    {
        if (sizeof($orderBy) === 0) {
            throw new InvalidArgumentException('Incorrect part: orderBy.');
        }

        $sql = $placeholders = [];
        foreach ($orderBy as list($field, $direction)) {
            if ($field instanceof Field) {
                $unit = $field->build();
                $placeholders = array_merge($placeholders, $unit->getPlaceholders());
                $field = $unit->getSql();
            }
            $sql[] = $field . ' ' . $direction;

        }

        return new BuilderUnit('ORDER BY ' . implode(', ', $sql), $placeholders);
    }

    protected function buildLimit(array $limit): BuilderUnit
    {
        if (sizeof($limit) === 0) {
            throw new InvalidArgumentException('Incorrect part: limit.');
        }

        $sql = 'LIMIT ?';
        $placeholders = [$limit[0]];
        if (sizeof($limit) === 2) {
            $sql .= ',?';
            $placeholders[] = $limit[1];
        }

        return new BuilderUnit($sql, $placeholders);
    }

    protected function buildUnion(array $union): BuilderUnit
    {
        if (sizeof($union) === 0) {
            throw new InvalidArgumentException('Incorrect part: union.');
        }

        $sql = $placeholders = [];
        foreach ($union as $query) {
            $unit = $query->build();
            $sql[] = 'UNION ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        return new BuilderUnit(implode(' ', $sql), $placeholders);
    }
}
