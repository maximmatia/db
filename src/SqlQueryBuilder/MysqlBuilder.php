<?php

namespace DB\SqlQueryBuilder;

use DB\Exception\InvalidArgumentException;
use DB\SqlQueryBuilder\BuilderInterface;
use DB\SqlQueryBuilder\BuilderUnit;
use DB\SqlQueryBuilder\Query\Elements\Conditions;
use DB\SqlQueryBuilder\Query\Elements\Field;
use DB\SqlQueryBuilder\Query\Elements\Insert;
use DB\SqlQueryBuilder\Query\Elements\Join;
use DB\SqlQueryBuilder\Query\Elements\Update;
use DB\SqlQueryBuilder\Query\SqlQuery;

class MysqlBuilder extends SqlBuilder
{
    private $having;
    private $updateMultiple;

    public function __construct(SqlQuery $query)
    {
        foreach ($query->toArray() as $key => $value) {
            if ($key == 'updateMultiple') {
                $this->updateMultiple = $value;
            } elseif ($key == 'having') {
                $this->having = $value;
            }
        };

        parent::__construct($query);
    }

    public function build(): BuilderUnit
    {
        $sql = '';
        $placeholders = [];

        if ($this->insert instanceof Insert) {
            $unit = $this->buildInsert($this->insert);
            $sql .= $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        } elseif ($this->updateMultiple instanceof Update) {
            $sql = 'UPDATE `' . $this->updateMultiple->getTable() . '`';
        } elseif (is_array($this->update) AND sizeof($this->update) > 0) {
            $sql = 'UPDATE';
        } elseif (!is_null($this->delete)) {
            $sql = 'DELETE ' . ($this->delete !== true ? $this->delete : '') . ' FROM';
        }

        #select
        if (is_array($this->select) AND sizeof($this->select) > 0) {
            $unit = $this->buildSelect($this->select);
            $sql .= (strlen($sql) > 0 ? ' ' : '') . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #from
        if (is_array($this->from) AND sizeof($this->from) > 0) {
            $unit = $this->buildFrom($this->from);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #join
        if (is_array($this->join) AND sizeof($this->join) > 0) {
            $unit = $this->buildJoin($this->join);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }


        #set
        if ($this->updateMultiple instanceof Update OR (is_array($this->update) AND sizeof($this->update) > 0)) {
            $update = [];
            if (is_array($this->update) AND sizeof($this->update) > 0) {
                $unit = $this->buildUpdate($this->update);
                $update[] = ' ' . $unit->getSql();
                $placeholders = array_merge($placeholders, $unit->getPlaceholders());
            }
            if ($this->updateMultiple instanceof Update) {
                $unit = $this->buildUpdateMultiple($this->updateMultiple);
                $update[] = ' ' . $unit->getSql();
                $placeholders = array_merge($placeholders, $unit->getPlaceholders());
            }
            $sql .= ' SET ' . implode(', ', $update);
        }

        #where
        if (is_array($this->where) AND sizeof($this->where) > 0) {
            $unit = $this->buildWhere($this->where);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #group by
        if (is_array($this->groupBy) AND sizeof($this->groupBy) > 0) {
            $unit = $this->buildGroupBy($this->groupBy);
            $sql .= ' ' . $unit->getSql();
        }

        #having
        if (is_array($this->having) AND sizeof($this->having) > 0) {
            $unit = $this->buildHaving($this->having);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #order by
        if (is_array($this->orderBy) AND sizeof($this->orderBy) > 0) {
            $unit = $this->buildOrderBy($this->orderBy);
            $sql .= ' ' . $unit->getSql();
        }

        #limit
        if (is_array($this->limit) AND sizeof($this->limit) > 0) {
            $unit = $this->buildLimit($this->limit);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        #union
        if (is_array($this->union) AND sizeof($this->union) > 0) {
            $unit = $this->buildUnion($this->union);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }


        if ($this->insert instanceof Insert AND sizeof($this->insert->getOnDuplicateKeyUpdate()) > 0) {
            $unit = $this->buildInsertDuplicateKeyUpdate($this->insert);
            $sql .= ' ' . $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());

        }

        return new BuilderUnit($sql, $placeholders);

    }

    protected function buildInsert(Insert $insert): BuilderUnit
    {
        $insert->setTable('`' . $insert->getTable() . '`');
        $insert->setFields(array_map(function ($field) {
            if ($field instanceof Field) {
                $field = $field->build()->getSql();
            }

            return '`' . $field . '`';
        }, $insert->getFields()));

        return parent::buildInsert($insert);
    }

    protected function buildInsertDuplicateKeyUpdate(Insert $insert): BuilderUnit
    {
        if (sizeof($insert->getOnDuplicateKeyUpdate()) > 0) {
            $insert->setOnDuplicateKeyUpdate(array_map(function ($value) {
                if ($value[0] instanceof Field) {
                    $value[0] = $value[0]->build()->getSql();
                }
                $value[0] = '`' . $value[0] . '`';

                return $value;

            }, $insert->getOnDuplicateKeyUpdate()));
        }

        return parent::buildInsertDuplicateKeyUpdate($insert);
    }

    protected function buildFrom(array $from): BuilderUnit
    {
        if (sizeof($from) === 0) {
            throw new InvalidArgumentException('Incorrect part: from.');
        }

        return parent::buildFrom(array_map(function ($item) {
            list($table, $alias) = $item;

            return ['`' . $table . '`', $alias];
        }, $from));
    }

    protected function buildJoin(array $join): BuilderUnit
    {
        if (sizeof($join) === 0) {
            throw new InvalidArgumentException('Incorrect part: join.');
        }

        return parent::buildJoin(array_map(function ($part) {
            if ($part instanceof Join AND !$part->getTable() instanceof BuilderInterface) {
                $part->setTable('`' . $part->getTable() . '`');
            }

            return $part;
        }, $join));
    }

    protected function buildHaving(array $having): BuilderUnit
    {
        if (sizeof($having) === 0) {
            throw new InvalidArgumentException('Incorrect part: having.');
        }

        $sql = $placeholders = [];
        foreach ($having as $conditions) {
            $unit = $conditions->build();
            $sql[] = $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        return new BuilderUnit('HAVING ' . implode(' AND ', $sql), $placeholders);
    }

    protected function buildUpdateMultiple(Update $update): BuilderUnit
    {
        $where = (new Conditions())->addBranches();

        $sql = $placeholders = $fieldSql = $fieldPlaceholders = [];
        foreach ($update->getUnique() as list($conditions, $values)) {
            if (is_array($conditions)) {
                $c = new Conditions();
                foreach ($conditions as $field => $value) {
                    $c->equal($field instanceof Field ? $field : new Field('`' . $field . '`'), $value);
                }
                $conditions = $c;
            } elseif (!$conditions instanceof Conditions) {
                throw new InvalidArgumentException('Incorrect conditions');
            }

            $where->branch()->with($conditions)->endBranch();
            $unit = $conditions->build();

            foreach ($values as $field => $value) {
                if ($field instanceof Field) {
                    $field = $field->build()->getSql();
                }

                $fieldSql[$field][] = "WHEN " . $unit->getSql() . " THEN ?";
                $fieldPlaceholders[$field] = array_merge($fieldPlaceholders[$field] ?? [],
                    $unit->getPlaceholders(), [$value]);
            }
        }
        $this->where[] = $where->endBranches();

        foreach ($fieldSql as $field => $data) {
            $sql[] = '`' . $field . '` = CASE ' . implode(' ', $data) . ' ELSE `' . $field . '` END';
            $placeholders = array_merge($placeholders, $fieldPlaceholders[$field]);
        }

        return new BuilderUnit(implode(', ', $sql), $placeholders);
    }


}