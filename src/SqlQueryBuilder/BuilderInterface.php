<?php

namespace DB\SqlQueryBuilder;

interface BuilderInterface
{
    public function build(): BuilderUnit;
}