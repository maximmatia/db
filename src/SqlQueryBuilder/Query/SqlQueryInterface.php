<?php

namespace DB\SqlQueryBuilder\Query;

use DB\Helper\Connection\Driver\Statement;
use DB\SqlQueryBuilder\Query\Elements\ConditionsWhere;
use DB\SqlQueryBuilder\Query\Elements\Insert;
use DB\SqlQueryBuilder\Query\Elements\Join;
use PDOStatement;

interface SqlQueryInterface
{
    public function from($tableName, $alias = null);

    public function select(...$fields);

    public function join($table, $alias = null): Join;

    public function joinLeft($table, $alias = null): Join;

    public function joinRight($table, $alias = null): Join;

    public function joinInner($table, $alias = null): Join;

    public function where(): ConditionsWhere;

    public function groupBy($field);

    public function orderBy($field, $direction = 'ASC');

    public function limit($amount, $offset = 0);

    public function delete($alias = true);

    public function update($field, $value);

    public function insert($table): Insert;

    public function union(MysqlQuery $query);
}