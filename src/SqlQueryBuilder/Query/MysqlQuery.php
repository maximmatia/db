<?php

namespace DB\SqlQueryBuilder\Query;

use DB\SqlQueryBuilder\MysqlBuilder;
use DB\SqlQueryBuilder\BuilderUnit;
use DB\SqlQueryBuilder\Query\Elements\ConditionsHaving;
use DB\SqlQueryBuilder\Query\Elements\Update;

class MysqlQuery extends SqlQuery
{
    private $having;
    private $updateMultiple;

    public function requireHaving(ConditionsHaving $conditions)
    {
        $this->having[] = $conditions;

        return $this;
    }

    public function having(): ConditionsHaving
    {
        return new ConditionsHaving($this);
    }

    public function updateMultiple($table): Update
    {
        return new Update($this, $table);
    }

    public function requireUpdateMultiple(Update $update)
    {
        $this->updateMultiple = $update;

        return $this;
    }

    public function toArray()
    {
        $o = parent::toArray();
        if (!is_null($this->having)) {
            $o['having'] = $this->having;
        }
        if (!is_null($this->updateMultiple)) {
            $o['updateMultiple'] = $this->updateMultiple;
        }

        return $o;
    }

    public function build(): BuilderUnit
    {
        return (new MysqlBuilder($this))->build();
    }
}