<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\SqlQueryBuilder\Query\SqlQuery;

class ConditionsOn extends Conditions
{
    private $join;

    public function __construct(Join $join)
    {
        $this->join = $join;
    }

    public function endOn(): SqlQuery
    {
        return $this->join->requireConditions($this)
            ->getQuery()
            ->requireJoin($this->join);

    }
}