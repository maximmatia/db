<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\SqlQueryBuilder\BuilderInterface;
use DB\SqlQueryBuilder\BuilderUnit;

class Value implements BuilderInterface
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function build(): BuilderUnit
    {
        return new BuilderUnit('?', [$this->value]);
    }
}
