<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\Exception\InvalidArgumentException;
use DB\SqlQueryBuilder\Query\MysqlQuery;

class Update
{
    private $query;
    private $table;
    private $unique = [];

    public function __construct(MysqlQuery $query, $table)
    {
        $this->query = $query;
        $this->table = $table;
    }

    public function unique($conditions, array $values)
    {
        if (sizeof($values) === 0) {
            throw new InvalidArgumentException();
        } elseif (!($conditions instanceof Conditions OR (is_array($conditions) AND sizeof($conditions) > 0))) {
            throw new InvalidArgumentException();
        }

        $this->unique[] = [$conditions, $values];

        return $this;
    }

    /**
     * @param array $data
     * [
     *      '0' => [
     *          '0' => [                                         # conditions
     *              'mainColumnName_1' => 'mainColumnValue_1'
     *              'mainColumnName_2' => 'mainColumnValue_2'
     *          ],
     *          '1' => [                                         # values
     *              'columnName_1' => 'columnValue_1'
     *              'columnName_2' => 'columnValue_2'
     *          ]
     *      ],
     *      '1' => [
     *          '0' => [
     *              'mainColumnName_1' => 'mainColumnValue_1'
     *              'mainColumnName_2' => 'mainColumnValue_2'
     *          ],
     *          '1' => [
     *              'columnName_1' => 'columnValue_1'
     *              'columnName_2' => 'columnValue_2'
     *          ]
     *      ]
     *      ...
     *  ]
     *
     * @return $this
     */
    public function uniqueMultiple(array $data)
    {
        if (sizeof($data) === 0) {
            throw new InvalidArgumentException();
        }
        $this->unique = array_merge($this->unique, $data);

        return $this;
    }

    public function common($field, $value)
    {
        $this->query->update($field instanceof Field ? $field : '`' . $field . '`', $value);

        return $this;
    }

    public function endUpdate(): MysqlQuery
    {
        return $this->query->requireUpdateMultiple($this);
    }

    /**
     * @return mixed
     */
    public function getUnique()
    {
        return $this->unique;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }
}