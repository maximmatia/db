<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\SqlQueryBuilder\Query\SqlQuery;

class ConditionsWhere extends Conditions
{
    private $query;

    public function __construct(SqlQuery $query)
    {
        $this->query = $query;
    }

    public function endWhere(): SqlQuery
    {
        return $this->query->requireWhere($this);
    }
}