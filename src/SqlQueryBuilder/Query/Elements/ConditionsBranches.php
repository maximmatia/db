<?php

namespace DB\SqlQueryBuilder\Query\Elements;

class ConditionsBranches extends Conditions
{
    private $branches;

    public function __construct(Branches $branches)
    {
        $this->branches = $branches;
    }

    public function endBranch(): Branches
    {
        return $this->branches->requireBranches($this);
    }
}