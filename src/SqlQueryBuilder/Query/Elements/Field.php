<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\SqlQueryBuilder\BuilderInterface;
use DB\SqlQueryBuilder\BuilderUnit;

class Field implements BuilderInterface
{
    private $placeholders = [];
    private $field;
    private $alias;

    public function __construct($field, $alias = null)
    {
        $this->field = $field;
        $this->alias = $alias;
    }

    public function addValue($value)
    {
        if ($value instanceof Value) {
            $value = array_pop($value->build()->getPlaceholders());
        }

        $this->placeholders[] = $value;

        return $this;
    }

    public function addValues(...$values)
    {
        $this->placeholders = array_merge($this->placeholders, array_map(function ($value) {
            if ($value instanceof Value) {
                $value = array_pop($value->build()->getPlaceholders());
            }

            return $value;
        }, $values));

        return $this;
    }

    public function build(): BuilderUnit
    {
        $o = $this->field . (!is_null($this->alias) ? " as '" . $this->alias . "'" : '');

        return new BuilderUnit($o, $this->placeholders);
    }
}
