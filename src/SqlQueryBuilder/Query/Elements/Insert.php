<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\Exception\InvalidArgumentException;
use DB\SqlQueryBuilder\Query\SqlQuery;

class Insert
{
    private $query;
    private $table;
    private $fields = [];
    private $values = [];
    private $onDuplicateKeyUpdate = [];

    public function __construct(SqlQuery $query, $table)
    {
        $this->query = $query;
        $this->table = $table;
    }

    public function fields(...$fields)
    {
        $this->fields = $fields;

        return $this;
    }

    public function values(array $values)
    {
        if (sizeof($values) === 0) {
            throw new InvalidArgumentException();
        }
        $this->values[] = array_map(function ($value) {
            if ($value instanceof Value) {
                $value = array_pop($value->build()->getPlaceholders());
            }

            return $value;
        }, $values);

        return $this;
    }

    /**
     * @param array $data
     * [
     *      '0' => [
     *          'columnName_1' => 'columnValue_1'
     *          'columnName_2' => 'columnValue_2'
     *           ...
     *      ],
     *      '1' => [
     *          'columnName_1' => 'columnValue_1'
     *          'columnName_2' => 'columnValue_2'
     *           ...
     *      ]
     *      ...
     *  ]
     *
     * @return $this
     */
    public function valuesMultiple(array $data)
    {
        if (sizeof($data) === 0) {
            throw new InvalidArgumentException();
        }
        $this->values = array_merge($this->values, $data);

        return $this;
    }

    public function onDuplicateKeyUpdate($field, $value = null)
    {
        if (!is_null($value) AND !is_object($value)) {
            $value = new Field($value);
        }

        if (!$field instanceof Field) {
            $field = new Field($field);
        }

        $this->onDuplicateKeyUpdate[] = [$field, $value];

        return $this;
    }

    public function endInsert(): SqlQuery
    {
        return $this->query->requireInsert($this);
    }

    /**
     * @param mixed $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param array $fields
     */
    public function setFields(array $fields)
    {
        $this->fields = $fields;
    }

    /**
     * @return mixed
     */
    public function getFields()
    {
        if (sizeof($this->fields) === 0 AND sizeof($this->values) > 0) {
            #get column names by first array item
            foreach ($this->values as $item) {
                $this->fields = array_keys($item);
                break;
            }
        }

        return $this->fields;
    }

    protected function getFieldsPure()
    {
        return array_map(function ($field) {
            if ($field instanceof Field) {
                $field = $field->build()->getSql();
            }

            return trim($field, '`');
        }, $this->getFields());
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        #checking data and sorting data
        $values = [];
        if (sizeof($this->values) > 0) {
            $fields = $this->getFieldsPure();
            if (sizeof($fields) > 0) {
                foreach ($this->values as $row) {
                    $rowFiltered = array_intersect_key($row, array_fill_keys($fields, 1));
                    if (sizeof($rowFiltered) === sizeof($fields)) {
                        $sorted = [];
                        foreach ($fields as $field) {
                            $sorted[] = $rowFiltered[$field];
                        }

                        $values[] = $sorted;
                    } else {
                        throw new InvalidArgumentException('Incorrect values.');
                    }
                }
            } else {
                $values = $this->values;
            }
        }


        return $values;
    }

    /**
     * @return array
     */
    public function getOnDuplicateKeyUpdate(): array
    {
        return $this->onDuplicateKeyUpdate;
    }

    /**
     * @param array $onDuplicateKeyUpdate
     */
    public function setOnDuplicateKeyUpdate(array $onDuplicateKeyUpdate)
    {
        $this->onDuplicateKeyUpdate = $onDuplicateKeyUpdate;
    }
}