<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\SqlQueryBuilder\Query\MysqlQuery;

class ConditionsHaving extends Conditions
{
    private $query;

    public function __construct(MysqlQuery $query)
    {
        $this->query = $query;
    }

    public function endHaving(): MysqlQuery
    {
        return $this->query->requireHaving($this);
    }
}