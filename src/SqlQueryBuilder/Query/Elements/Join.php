<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\SqlQueryBuilder\Query\SqlQuery;

class Join
{
    private $query;
    private $table;
    private $alias;
    private $type;
    private $conditions;

    public function __construct(SqlQuery $query, $table, $alias, $type = 'JOIN')
    {
        $this->query = $query;
        $this->table = $table;
        $this->alias = $alias;
        $this->type = $type;
    }

    public function requireConditions(ConditionsOn $conditions)
    {
        $this->conditions = $conditions;

        return $this;
    }

    public function on(): ConditionsOn
    {
        return new ConditionsOn($this);

    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param $table
     *
     * @return $this
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @return null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @return SqlQuery
     */
    public function getQuery(): SqlQuery
    {
        return $this->query;
    }
}
