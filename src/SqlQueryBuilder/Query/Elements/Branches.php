<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\SqlQueryBuilder\BuilderInterface;
use DB\SqlQueryBuilder\BuilderUnit;

class Branches implements BuilderInterface
{
    private $conditions;
    private $branches = [];

    public function __construct(Conditions $conditions)
    {
        $this->conditions = $conditions;
    }

    public function build(): BuilderUnit
    {
        $sql = $placeholders = [];
        foreach ($this->branches as $conditions) {
            $unit = $conditions->build();
            $sql[] = $unit->getSql();
            $placeholders = array_merge($placeholders, $unit->getPlaceholders());
        }

        return new BuilderUnit('((' . implode(') OR (', $sql) . '))', $placeholders);
    }


    public function requireBranches(ConditionsBranches $conditions)
    {
        $this->branches[] = $conditions;

        return $this;
    }

    public function branch():ConditionsBranches
    {
        return new ConditionsBranches($this);
    }

    public function endBranches(): Conditions
    {
        $unit = $this->build();

        return $this->conditions->requireBranches($unit);
    }
}