<?php

namespace DB\SqlQueryBuilder\Query\Elements;

use DB\SqlQueryBuilder\BuilderInterface;
use DB\SqlQueryBuilder\BuilderUnit;
use DB\Exception\InvalidArgumentException;
use DB\SqlQueryBuilder\Query\SqlQuery;

class Conditions implements BuilderInterface
{
    private $sql          = [];
    private $placeholders = [];
    private $bindValues = [];

    public function build(): BuilderUnit
    {
        return new BuilderUnit(implode(' AND ', $this->sql), $this->placeholders, $this->bindValues);
    }

    public function with(Conditions $conditions)
    {
        $unit = $conditions->build();
        $this->sql[] = $unit->getSql();
        $this->placeholders = array_merge($this->placeholders, $unit->getPlaceholders());

        return $this;
    }

    public function addBranches(): Branches
    {
        return new Branches($this);
    }

    public function requireBranches(BuilderUnit $unit)
    {
        $this->sql[] = $unit->getSql();
        $this->placeholders = array_merge($this->placeholders, $unit->getPlaceholders());

        return $this;
    }

    public function equalField($field1, $filed2)
    {
        return $this->condition($field1, '=', new Field($filed2));
    }

    public function equal($field1, $value)
    {
        return $this->condition($field1, '=', $value);
    }

    public function notEqual($field1, $value)
    {
        return $this->condition($field1, '!=', $value);
    }

    public function greaterThan($field1, $value)
    {
        return $this->condition($field1, '>', $value);
    }

    public function greaterThanOrEqual($field1, $value)
    {
        return $this->condition($field1, '>=', $value);
    }

    public function lessThan($field1, $value)
    {
        return $this->condition($field1, '<', $value);
    }

    public function lessThanOrEqual($field1, $value)
    {
        return $this->condition($field1, '<=', $value);
    }


    /**
     * @param $field
     * @param $condition
     * @param $value
     *
     * @return $this
     */
    public function condition($field, $condition, $value)
    {
        if (!array_key_exists($condition, array_flip(['>', '<', '=', '>=', '<=', '!=', '<>']))) {
            throw new InvalidArgumentException('Incorrect condition.');
        }
        $field = $this->extractField($field);
        if ($value instanceof Field) {
            $value = $this->extractField($value);
            $this->sql[] = $field->getSql() . ' ' . $condition . ' ' . $value->getSql();
            $this->placeholders = array_merge(
                $this->placeholders,
                $field->getPlaceholders(),
                $value->getPlaceholders()
            );
        } else {
            if ($value instanceof Value) {
                $array = $value->build()->getPlaceholders();
                $value = array_pop($array);
            }
            $this->sql[] = $field->getSql() . ' ' . $condition . ' ?';
            $this->placeholders = array_merge($this->placeholders, $field->getPlaceholders(), [$value]);
        }


        return $this;
    }


    public function between($field, $value1, $value2)
    {
        $field = $this->extractField($field);
        if ($value1 instanceof Value) {
            $array1 = $value1->build()->getPlaceholders();
            $value1 = array_pop($array1);
        }
        if ($value2 instanceof Value) {
            $array2  = $value2->build()->getPlaceholders();
            $value2 = array_pop($array2);
        }

        $this->sql[] = $field->getSql() . ' BETWEEN ? AND ?';
        $this->placeholders = array_merge($this->placeholders, [$value1, $value2]);

        return $this;
    }

    public function notBetween($field, $value1, $value2)
    {
        $field = $this->extractField($field);
        if ($value1 instanceof Value) {
            $array1  = $value1->build()->getPlaceholders();
            $value1 = array_pop($array1);
        }
        if ($value2 instanceof Value) {
            $array2  = $value2->build()->getPlaceholders();
            $value2 = array_pop($array2);
        }

        $this->sql[] = $field->getSql() . ' NOT BETWEEN ? AND ?';
        $this->placeholders = array_merge($this->placeholders, [$value1, $value2]);

        return $this;
    }
    public function like($field, $value)
    {
        $field = $this->extractField($field);
        if ($value instanceof Value) {
            $array  = $value->build()->getPlaceholders();
            $value = array_pop($array);
        }

        $this->sql[] = $field->getSql() . ' LIKE ? ';
        $this->placeholders = array_merge($this->placeholders, [$value]);

        return $this;
    }
    public function notLike($field, $value)
    {
        $field = $this->extractField($field);
        if ($value instanceof Value) {
            $array  = $value->build()->getPlaceholders();
            $value = array_pop($array);
        }

        $this->sql[] = $field->getSql() . ' NOT LIKE ? ';
        $this->placeholders = array_merge($this->placeholders, [$value]);

        return $this;
    }

    public function in($field, $value)
    {
        $field = $this->extractField($field);
        if (is_array($value)) {
            if (sizeof($value) > 0) {
                $this->sql[] = $field->getSql() . ' IN (' . implode(', ', array_fill_keys($value, '?')) . ')';
                $this->placeholders = array_merge($this->placeholders, array_values($value));
            }

            return $this;
        } elseif ($value instanceof BuilderInterface) {
            $unit = $value->build();
            $this->sql[] = $field->getSql() . ' IN (' . $unit->getSql() . ')';
            $this->placeholders = array_merge($this->placeholders, $unit->getPlaceholders());

            return $this;
        }

        throw new InvalidArgumentException('Incorrect value.');
    }

    public function notIn($field, $value)
    {
        $field = $this->extractField($field);
        if (is_array($value)) {
            if (sizeof($value) > 0) {
                $this->sql[] = $field->getSql() . ' NOT IN (' . implode(', ', array_fill_keys($value, '?')) . ')';
                $this->placeholders = array_merge($this->placeholders, array_values($value));
            }

            return $this;
        } elseif ($value instanceof BuilderInterface) {
            $unit = $value->build();
            $this->sql[] = $field->getSql() . ' NOT IN (' . $unit->getSql() . ')';
            $this->placeholders = array_merge($this->placeholders, $unit->getPlaceholders());

            return $this;
        }

        throw new InvalidArgumentException('Incorrect value.');
    }

    public function isNull($field)
    {
        $field = $this->extractField($field);
        $this->sql[] = $field->getSql() . ' IS NULL';

        return $this;
    }

    public function isNotNull($field)
    {
        $field = $this->extractField($field);
        $this->sql[] = $field->getSql() . ' IS NOT NULL';

        return $this;
    }

    protected function extractField($field): BuilderUnit
    {
        if (is_string($field)) {
            $field = new Field($field);
        }

        if ($field instanceof Field or $field instanceof SqlQuery) {
            return $field->build();
        }

        throw new InvalidArgumentException('Incorrect field.');
    }
}
