<?php

namespace DB\SqlQueryBuilder\Query;

use DB\Exception\DBException;
use DB\Helper\Connection\Driver\Statement;
use DB\SqlQueryBuilder\SqlBuilder;
use DB\SqlQueryBuilder\BuilderInterface;
use DB\SqlQueryBuilder\BuilderUnit;
use DB\SqlQueryBuilder\Query\Elements\ConditionsWhere;
use DB\SqlQueryBuilder\Query\Elements\Field;
use DB\SqlQueryBuilder\Query\Elements\Insert;
use DB\SqlQueryBuilder\Query\Elements\Join;
use DB\SqlQueryBuilder\Query\Elements\Value;
use DB\Wrapper\SqlWrapperInterface;

class SqlQuery implements BuilderInterface, SqlQueryInterface
{
    protected $wrapper;
    protected $from;
    protected $select;
    protected $join;
    protected $where;
    protected $groupBy;
    protected $orderBy;
    protected $limit;
    protected $delete;
    protected $update;
    protected $insert;
    protected $union;

    public function __construct(SqlWrapperInterface $wrapper = null)
    {
        $this->wrapper = $wrapper;
    }

    public function requireJoin(Join $join)
    {
        $this->join[] = $join;

        return $this;
    }

    public function requireWhere(ConditionsWhere $conditions)
    {
        $this->where[] = $conditions;

        return $this;
    }

    public function requireInsert(Insert $insert)
    {
        $this->insert = $insert;

        return $this;
    }

    public function from($table, $alias = '')
    {
        $this->from[] = [$table, $alias];

        return $this;
    }

    public function select(...$fields)
    {
        $this->select = array_merge(array_map(function ($field) {
            return $field instanceof Field ? $field : new Field($field);
        }, $fields), is_array($this->select) ? $this->select : []);

        return $this;
    }

    public function join($table, $alias = ''): Join
    {
        return new Join($this, $table, $alias, 'JOIN');
    }

    public function joinInner($table, $alias = ''): Join
    {
        return new Join($this, $table, $alias, 'INNER JOIN');
    }

    public function joinLeft($table, $alias = ''): Join
    {
        return new Join($this, $table, $alias, 'LEFT JOIN');
    }

    public function joinRight($table, $alias = ''): Join
    {
        return new Join($this, $table, $alias, 'RIGHT JOIN');
    }

    public function where(): ConditionsWhere
    {
        return new ConditionsWhere($this);
    }

    public function groupBy($field)
    {
        $this->groupBy[] = $field;

        return $this;
    }

    public function orderBy($field, $direction = 'ASC')
    {
        $this->orderBy[] = [$field, $direction];

        return $this;
    }

    public function limit($amount, $offset = 0)
    {
        $this->limit = ($offset === 0 ? [$amount] : [$offset, $amount]);

        return $this;
    }

    public function delete($alias = true)
    {
        $this->delete = $alias;

        return $this;
    }

    public function update($field, $value)
    {
        if ($value instanceof Value) {
            $value = array_pop($value->build()->getPlaceholders());
        }

        $this->update[] = [$field, $value];

        return $this;
    }


    public function insert($table): Insert
    {
        return new Insert($this, $table);
    }

    public function union(MysqlQuery $query)
    {
        $this->union[] = $query;

        return $this;
    }

    public function toArray()
    {
        $o = [];
        if (!is_null($this->from)) {
            $o['from'] = $this->from;
        }
        if (!is_null($this->select)) {
            $o['select'] = $this->select;
        }
        if (!is_null($this->join)) {
            $o['join'] = $this->join;
        }
        if (!is_null($this->where)) {
            $o['where'] = $this->where;
        }
        if (!is_null($this->groupBy)) {
            $o['groupBy'] = $this->groupBy;
        }
        if (!is_null($this->orderBy)) {
            $o['orderBy'] = $this->orderBy;
        }
        if (!is_null($this->limit)) {
            $o['limit'] = $this->limit;
        }
        if (!is_null($this->delete)) {
            $o['delete'] = $this->delete;
        }
        if (!is_null($this->update)) {
            $o['update'] = $this->update;
        }
        if (!is_null($this->insert)) {
            $o['insert'] = $this->insert;
        }
        if (!is_null($this->union)) {
            $o['union'] = $this->union;
        }

        return $o;
    }

    public function build(): BuilderUnit
    {
        return (new SqlBuilder($this))->build();
    }

    public function setWrapper(SqlWrapperInterface $wrapper)
    {
        $this->wrapper = $wrapper;

        return $this;
    }

    /**
     * @return \PDOStatement
     * @throws \Exception
     */
    public function executeQuery(): \PDOStatement
    {
        try {
            if (is_null($this->wrapper)) {
                throw new DBException('Wrapper is not set.');
            }
            $unit = $this->build();

            return $this->wrapper->executeQuery($unit->getSql(), $unit->getPlaceholders());
        } catch (\Exception $e) {
            if (substr_count($e->getMessage(), 'Deadlock found')) {
                sleep(1);
                return $this->executeQuery();
            }

            throw $e;
        }



    }
}
